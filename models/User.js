const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

    firstName: {
        type: String,
        required: [true, "First Name is Required"]
    },
    lastName: {
        type: String,
        required: [true, "Last Name is Required"]
    },
    mobileNo: {
        type: String,
        required: [true, "Mobile Number is Required"]
    },
    email: {
        type: String,
        required: [true, "Email is Required"]
    },
    password: {
        type: String,
        required: [true, "Password is Required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    enrollments: [

        {
            courseId: {
                type: String,
                required: [true, "Course ID is required"]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                default: "Enrolled"
            }
        }

    ]

});

module.exports = mongoose.model("User", userSchema);