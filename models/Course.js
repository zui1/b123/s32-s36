const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({

    name: {
        type: String,
        required: [true, "Name is Required"]
    },
    description: {
        type: String,
        required: [true, "Description is Required"]
    },
    price: {
        type: Number,
        required: [true, "Price is Required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    /*
        In mogoDB, we can have 2 way embedding for models with many to many relationship.
        In mongoDB, to implement 2 way embedding we add subdocument arrays on both models.
        Each subdocument array also has a schema for its subdocuments.
    */
    enrollees: [

        {
            userId: {
                type: String,
                required: [true, "User Id is required."]
            },
            enrolledOn: {
                type: Date,
                default: new Date()
            },
            status: {
                type: String,
                default: "Enrolled"
            }
        }

    ]

});

module.exports = mongoose.model("Course", courseSchema);