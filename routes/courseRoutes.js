const express = require("express");

const router = express.Router();

const courseControllers = require('../controllers/courseControllers');

const {
    addCourse,
    getAllCourse,
    getAllActiveCourse,
    getSingleCourse,
    updateCourse,
    archiveCourse,
    activateCourse,
    getEnrollees
} = courseControllers

const auth = require('../auth');
const { verify, verifyAdmin } = auth;

router.post('/', verify, verifyAdmin, addCourse);

router.get('/', verify, verifyAdmin, getAllCourse);

router.get('/getActiveCourses', getAllActiveCourse);

router.get('/getSingleCourse/:id', getSingleCourse);

router.put("/:id", verify, verifyAdmin, updateCourse);

router.put('/archive/:id', verify, verifyAdmin, archiveCourse);

router.put('/activate/:id', verify, verifyAdmin, activateCourse);

router.get('/getEnrollees/:id', verify, verifyAdmin, getEnrollees)

module.exports = router;