/* We will create a module which will have functions and methods that will help us authenticate our users to either give them permission or restrict them from an action. To do this, first we need to give our users a key to access our app.*/

const jwt = require('jsonwebtoken');
const secret = "CourseBookingApi";

/*
	JWT is a way to securely pass information from a part of a server to the frontend or other parts of the server. It allows us to create a sort of "keys" which is able to authenticate our user.

	The secret passed is any string but to allow access with the token, the secret must be intact.

	JWT is like a gift wrapping service but with a secret and only our system knows about this secret. And if the secret is not intact or the JWT seems tampered with we will be able to reject the use who used an illegitimate Token.

	This will ensure that only registered users can do certain things in our app.
*/

module.exports.createAccessToken = (user) => {

    //console.log(user);
    //data object is created to contain some details of our user.
    //Only a logged in user is handed or given a token.
    const data = {

        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin

    };

    //create your jwt with the payload, with the secret, the algorithm to create your JWT
    return jwt.sign(data, secret, {});

}

module.exports.verify = (req, res, next) => {

    //Token passed as bearer tokens can be found in which part of your request?
    //console.log(req.headers.authorization)
    //Token variable will hold our bearer token from our client.
    let token = req.headers.authorization
    //console.log(token);
    //if no token is passed, req.headers.authorization is undefined.
    if (typeof token === "undefined") {

        return res.send({ auth: "Failed. No Token" });

    } else {

        //extract the token and remove the "Bearer" from our token variable.
        token = token.slice(7, token.length);
        //console.log(token);

        //verify the legitimacy of your token:
        jwt.verify(token, secret, function(err, decodedToken) {

            if (err) {
                //err will contain the error from our decoding our token
                //decodedToken is our token after completing and accomplishing verification of its legitimacy against our secret.
                return res.send({
                    auth: "Failed",
                    message: err.message
                });

            } else {

                console.log(decodedToken); //contains the data payload from our token
                //We will add a new property in the req object called user. Assign the decoded data to that property.
                req.user = decodedToken;
                //next() will allow to run the next function. (another Middleware or the controller)
                next()

            }

        })

    }

}

module.exports.verifyAdmin = (req, res, next) => {

    if (req.user.isAdmin) {

        next()

    } else {

        return res.send({

            auth: "Failed",
            message: "Action Forbidden"

        })

    }

}