const Course = require('../models/Course');

module.exports.addCourse = (req, res) => {

    //console.log(req.body);

    let newCourse = new Course({

        name: req.body.name,
        description: req.body.description,
        price: req.body.price,

    })

    //always return your res.send()
    //res.send() should always be last process done. No other steps within the code block should be done after res.send
    newCourse.save()
        .then(result => res.send(result))
        .catch(err => res.send(err))


}

module.exports.getAllCourse = (req, res) => {

    Course.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.getAllActiveCourse = (req, res) => {

    Course.find({ isActive: true })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.getSingleCourse = (req, res) => {

    Course.findById(req.params.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.updateCourse = (req, res) => {

    let updates = {

        name: req.body.name,
        description: req.body.description,
        price: req.body.price

    }

    Course.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.archiveCourse = (req, res) => {

    let update = {

        isActive: false

    }

    Course.findByIdAndUpdate(req.params.id, update, { new: true })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.activateCourse = (req, res) => {

    let update = {

        isActive: true

    }

    Course.findByIdAndUpdate(req.params.id, update, { new: true })
        .then(result => res.send(result))
        .catch(err => res.send(err))

}

module.exports.getEnrollees = (req, res) => {

       if (req.user.isAdmin !== true) return res.send({

        auth: "Failed",
        message: "Action Forbidden"

    });

        Course.findById(req.params.id)
        .then(course => {
            res.send(course.enrollees)
        })
        

        .catch(err => res.send(err))


}